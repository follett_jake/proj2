from flask import Flask, render_template, request
import os

app = Flask(__name__)

@app.route("/<name>")
def hello(name):
    #check if the file is forbidden or not
    if "//" in name or "~" in name or ".." in name:
        abort(403)

    #this will get the destination for the file
    destination = "pages/" + name
    
    #check if the file is found or not, if there is no file get 404
    if os.path.isfile(destination):
        return render_template(name)
    else:
        abort(404)

    #error handling for 403
    @app.errorhandler(403)
    def notfound(error):
        return render_template("403.html"), 403
    
    #error handling for 404
    @app.errorhandler(404)
    def forbidden(error):
        return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
